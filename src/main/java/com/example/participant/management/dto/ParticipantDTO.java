package com.example.participant.management.dto;
import com.example.participant.management.validation.bicvalidator.UniqueBic;
import com.example.participant.management.validation.codevalidator.UniqueCode;
import com.example.participant.management.validation.settlementbankvalidator.ValidSettlementBank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class ParticipantDTO {

    @NotNull
    @Length(min = 6, max = 6)
    @UniqueCode
    private String code;

    @NotNull
    @Pattern(regexp = "^[A-Za-z]{4}[A-Za-z]{2}[A-Za-z0-9]{2}[A-Za-z0-9]{3}$")
    @UniqueBic
    private String bic;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9 ]*$", message = "Name should be alphanumeric")
    private String name;

    @NotNull
    @Pattern(regexp = "^[A-Z ]*$", message = "Short name should be uppercase English letters")
    private String shortName;

    @NotNull
    @Pattern(regexp = "Direct|Indirect", message = "Type should be either 'Direct' or 'Indirect'")
    private String type;

    private String logo;

    @ValidSettlementBank
    private String settlementBank;
}
