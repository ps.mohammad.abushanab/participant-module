package com.example.participant.management.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Participant {

    @Id
    @Column(unique = true, nullable = false)
    private String code;

    @Column(unique = true, nullable = false)
    private String bic;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String shortName;

    @Column(nullable = false)
    private String type;

    private String logo;

    private String settlementBank;

}
