package com.example.participant.management.validation;

import com.example.participant.management.dto.UserDTO;
import com.example.participant.management.exception.InvalidCredentialsException;
import com.example.participant.management.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.example.participant.management.util.UserMapperUtil.convertEntityToDto;

@Component
public class LoginValidation {

    @Autowired
    UserRepository userRepository;

    public UserDTO validateLoginInput(String username, String password) {
        if (username.isEmpty()|| password.isEmpty()) {
            throw new InvalidCredentialsException("Username and password must not be empty");
        }
        UserDTO user;
        try {
             user = convertEntityToDto(userRepository.findByUsernameAndPassword(username, password));
        }catch (Exception e){
            throw new InvalidCredentialsException("Invalid username or password");
        }

        return user;

    }
}
