package com.example.participant.management.validation.settlementbankvalidator;

import com.example.participant.management.model.Participant;
import com.example.participant.management.repository.ParticipantRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class SettlementBankValidator implements ConstraintValidator<ValidSettlementBank, String> {
    @Override
    public void initialize(ValidSettlementBank constraintAnnotation) {}

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public boolean isValid(String settlementBank, ConstraintValidatorContext context) {

           if( settlementBank != null) {
               Participant participant = participantRepository.findByCode(settlementBank);
               if (participant == null) {
                   return false;
               }
               if ("Direct".equalsIgnoreCase(participant.getType())) {
                   return true;
               }


           }
        return true;

    }
}
