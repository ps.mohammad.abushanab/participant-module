package com.example.participant.management.validation.bicvalidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueBicValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueBic {
    String message() default "BIC must be unique";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
