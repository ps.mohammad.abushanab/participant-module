package com.example.participant.management.validation.settlementbankvalidator;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = SettlementBankValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidSettlementBank {
    String message() default "Invalid settlement bank value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
