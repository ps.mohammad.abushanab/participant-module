package com.example.participant.management.validation.bicvalidator;

import com.example.participant.management.repository.ParticipantRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class UniqueBicValidator implements ConstraintValidator<UniqueBic, String> {

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public boolean isValid(String bic, ConstraintValidatorContext context) {
        return participantRepository.findByBic(bic) == null;
    }
}
