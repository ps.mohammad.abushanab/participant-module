package com.example.participant.management.validation;

import com.example.participant.management.dto.ParticipantDTO;
import com.example.participant.management.exception.ParticipantDeletionException;
import com.example.participant.management.exception.ParticipantExistException;
import com.example.participant.management.exception.ParticipantNotFoundException;
import com.example.participant.management.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import static com.example.participant.management.util.ParticipantMapperUtil.convertEntityToDto;

@Component
public class ParticipantValidation {

    @Autowired
    private ParticipantRepository participantRepository;

    public void validateParticipantCodeExists(String code) {
        if (participantRepository.findByCode(code) == null) {
            throw new ParticipantNotFoundException("Participant with code" + code + " not found");
        }
    }

    public void validateNewParticipant(String code) {
        if (participantRepository.findByCode(code) != null) {
            throw new ParticipantExistException("Participant with code " + code + " is Exist");
        }
    }

    public void validateParticipantCanBeDeleted(String code) {

        try {
            ParticipantDTO participant = convertEntityToDto(participantRepository.findByCode(code));
            if (participant == null) {
                throw new ParticipantNotFoundException("Participant with code " + code + " not found");
            }
        }catch (Exception e){
                throw new ParticipantNotFoundException("Participant with code " + code + " not found");
            }

        try {
            boolean isReferenced = participantRepository.findAll().stream()
                    .anyMatch(p -> "Indirect".equalsIgnoreCase(p.getType()) && code.equals(p.getSettlementBank()));

            if (isReferenced) {
                throw new ParticipantDeletionException("Cannot delete participant with code " + code + " as it is referenced by an indirect participant");
            }
        }catch (Exception e){
            throw new ParticipantDeletionException("Cannot delete participant with code " + code + " as it is referenced by an indirect participant");

        }



    }

    public List<ParticipantDTO> validateParticipantsListNotEmpty(List<ParticipantDTO> participants) {
        if (participants.isEmpty()) {
            throw new ParticipantNotFoundException("No participants found");
        }
        return participants;
    }


}