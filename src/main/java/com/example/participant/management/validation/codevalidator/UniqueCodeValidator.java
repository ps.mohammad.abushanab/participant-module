package com.example.participant.management.validation.codevalidator;


import com.example.participant.management.repository.ParticipantRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;



public class UniqueCodeValidator implements ConstraintValidator<UniqueCode, String> {

    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public boolean isValid(String code, ConstraintValidatorContext context) {
        return participantRepository.findByCode(code) == null;
    }
}
