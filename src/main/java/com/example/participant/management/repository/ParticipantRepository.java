package com.example.participant.management.repository;

import com.example.participant.management.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, String> {
    Participant findByCode(String code);
    Participant findByBic(String bic);
    List<Participant> findAll();


}
