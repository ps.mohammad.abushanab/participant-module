package com.example.participant.management.util;

import com.example.participant.management.dto.ParticipantDTO;
import com.example.participant.management.model.Participant;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import java.util.List;
import java.util.stream.Collectors;

public class ParticipantMapperUtil {
    private static ModelMapper modelMapper = new ModelMapper();

    static {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
    }

    public static ParticipantDTO convertEntityToDto(Participant participant){
        return modelMapper.map(participant, ParticipantDTO.class);
    }

    public static List<ParticipantDTO> convertEntitiesToDto(List<Participant> participants){
        return participants.stream()
                .map(participant -> convertEntityToDto(participant))
                .collect(Collectors.toList());
    }

    public static Participant convertDtoToEntity(ParticipantDTO participantDTO){
        return modelMapper.map(participantDTO, Participant.class);
    }
}
