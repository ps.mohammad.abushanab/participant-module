package com.example.participant.management.util;

import com.example.participant.management.dto.UserDTO;
import com.example.participant.management.model.User;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class UserMapperUtil {

    private static ModelMapper modelMapper = new ModelMapper();

    static {
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
    }

    public static UserDTO convertEntityToDto(User user) {
        return modelMapper.map(user, UserDTO.class);
    }
}
