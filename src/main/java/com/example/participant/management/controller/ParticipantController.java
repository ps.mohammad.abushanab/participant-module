package com.example.participant.management.controller;

import com.example.participant.management.dto.ParticipantDTO;
import java.util.List;
import com.example.participant.management.service.ParticipantService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping({"/api/participants"})
public class ParticipantController {

    @Autowired
    private ParticipantService participantService;

    @PostMapping
    public ResponseEntity<Void> createParticipant(@Valid @RequestBody ParticipantDTO participant) {
        participantService.createParticipant(participant);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{code}")
    public ResponseEntity<Void> updateParticipant(@PathVariable String code, @Valid @RequestBody ParticipantDTO participantDetails) {
        participantService.updateParticipant(code, participantDetails);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<ParticipantDTO>> getAllParticipants() {
        List<ParticipantDTO> participants = participantService.getAllParticipants();
        return ResponseEntity.ok(participants);
    }

    @GetMapping("/{code}")
    public ResponseEntity<ParticipantDTO> getParticipantByCode(@PathVariable String code) {
        ParticipantDTO participant = participantService.getParticipantByCode(code);
        return ResponseEntity.ok(participant);
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<Void> deleteParticipant(@PathVariable String code) {
        participantService.deleteParticipant(code);
        return ResponseEntity.ok().build();
    }
}
