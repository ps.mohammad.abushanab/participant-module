package com.example.participant.management.controller;
import com.example.participant.management.dto.UserDTO;
import com.example.participant.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@RequestBody UserDTO loginUser) {
       UserDTO userDTO= userService.login(loginUser.getUsername(), loginUser.getPassword());
        return ResponseEntity.ok(userDTO) ;
    }
}
