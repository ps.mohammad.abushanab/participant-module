package com.example.participant.management.exception;

public class ParticipantValidationException extends RuntimeException {
    public ParticipantValidationException(String message) {
        super(message);
    }
}
