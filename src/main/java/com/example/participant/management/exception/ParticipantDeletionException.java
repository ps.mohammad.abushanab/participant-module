package com.example.participant.management.exception;

public class ParticipantDeletionException extends RuntimeException {
    public ParticipantDeletionException(String message) {
        super(message);
    }
}
