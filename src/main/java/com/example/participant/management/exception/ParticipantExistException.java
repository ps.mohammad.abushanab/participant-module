package com.example.participant.management.exception;

public class ParticipantExistException extends RuntimeException {
    public ParticipantExistException(String message) {
        super(message);
    }
}
