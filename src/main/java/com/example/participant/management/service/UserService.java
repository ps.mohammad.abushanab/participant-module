package com.example.participant.management.service;

import com.example.participant.management.dto.UserDTO;
import com.example.participant.management.exception.InvalidCredentialsException;
import com.example.participant.management.repository.UserRepository;
import com.example.participant.management.validation.LoginValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoginValidation loginValidation;

    public UserDTO login(String username, String password) {
            return loginValidation.validateLoginInput(username, password);
    }

}
