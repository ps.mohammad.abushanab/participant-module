package com.example.participant.management.service;

import com.example.participant.management.dto.ParticipantDTO;
import com.example.participant.management.repository.ParticipantRepository;

import java.util.List;

import com.example.participant.management.validation.ParticipantValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.example.participant.management.util.ParticipantMapperUtil.*;

@Service
public class ParticipantService {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private ParticipantValidation participantValidation;


    public void createParticipant(ParticipantDTO newParticipantDetails) {
        participantValidation.validateNewParticipant(newParticipantDetails.getCode());
        participantRepository.save(convertDtoToEntity(newParticipantDetails));
    }

    public void updateParticipant(String code, ParticipantDTO participantDetails) {
        participantValidation.validateParticipantCodeExists(code);
        participantRepository.save(convertDtoToEntity(participantDetails));
    }

    public List<ParticipantDTO> getAllParticipants() {
        List<ParticipantDTO> participants = convertEntitiesToDto(participantRepository.findAll());
        participantValidation.validateParticipantsListNotEmpty(participants);
        return participants;
    }

    public ParticipantDTO getParticipantByCode(String code) {
        participantValidation.validateParticipantCodeExists(code);
        return convertEntityToDto(participantRepository.findByCode(code));
    }

    public void deleteParticipant(String code) {
        participantValidation.validateParticipantCanBeDeleted(code);
        participantRepository.delete(participantRepository.findByCode(code));
    }


}
