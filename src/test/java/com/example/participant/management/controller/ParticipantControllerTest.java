package com.example.participant.management.controller;

import com.example.participant.management.dto.ParticipantDTO;
import com.example.participant.management.repository.ParticipantRepository;
import com.example.participant.management.service.ParticipantService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import java.util.Collections;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(ParticipantController.class)
public class ParticipantControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ParticipantService participantService;

    @MockBean
    private ParticipantRepository participantRepository;

    @InjectMocks
    private ParticipantController participantController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void when_create_valid_participant_should_return_ok() throws Exception {
        ParticipantDTO participant = new ParticipantDTO();
        participant.setCode("PART05");
        participant.setBic("AbCdEf12Gh3");
        participant.setName("Example Company 123");
        participant.setShortName("EXAMPLE");
        participant.setType("Direct");
        participant.setSettlementBank(null);

        doNothing().when(participantService).createParticipant(participant);

        ResultActions resultActions = mockMvc.perform(post("/api/participants")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(participant)));

        resultActions.andExpect(status().isOk());
    }


    @Test
    public void when_get_all_participants_should_return_list_of_participants() throws Exception {
        when(participantService.getAllParticipants()).thenReturn(Collections.emptyList());

        ResultActions resultActions = mockMvc.perform(get("/api/participants"));

        resultActions.andExpect(status().isOk());
    }

    @Test
    public void when_get_valid_participant_by_code_should_return_data_for_participant() throws Exception {
        String code = "PART01";
        ParticipantDTO participantDTO = createParticipantDTO();

        when(participantService.getParticipantByCode(code)).thenReturn(participantDTO);

        ResultActions resultActions = mockMvc.perform(get("/api/participants/{code}", code));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.code").value(participantDTO.getCode()))
                .andExpect(jsonPath("$.bic").value(participantDTO.getBic()))
                .andExpect(jsonPath("$.name").value(participantDTO.getName()))
                .andExpect(jsonPath("$.shortName").value(participantDTO.getShortName()))
                .andExpect(jsonPath("$.type").value(participantDTO.getType()))
                .andExpect(jsonPath("$.settlementBank").value(participantDTO.getSettlementBank()));
    }

    @Test
    public void when_delete_exist_participant_should_return_ok() throws Exception {
        String code = "PART01";
        doNothing().when(participantService).deleteParticipant(code);
        ResultActions resultActions = mockMvc.perform(delete("/api/participants/{code}", code));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void when_create_participant_with_invalid_data() throws Exception {
        ParticipantDTO participantDTO = new ParticipantDTO();
        participantDTO.setCode("InvalidCode");


        ResultActions resultActions = mockMvc.perform(post("/api/participants")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(participantDTO)));

        resultActions.andExpect(status().isBadRequest());
    }

    @Test
    public void when_update_participant_with_invalid_data() throws Exception {
        String code = "InvalidCode";
        ParticipantDTO participantDTO = new ParticipantDTO();
        participantDTO.setCode(code);
        participantDTO.setBic("AAAABBCC123");
        participantDTO.setName("Example Participant");
        participantDTO.setShortName("EP");
        participantDTO.setType("Direct");
        participantDTO.setSettlementBank(null);

        ResultActions resultActions = mockMvc.perform(put("/api/participants/{code}", code)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(participantDTO)));

        resultActions.andExpect(status().isBadRequest());
    }

    private ParticipantDTO createParticipantDTO() {
        ParticipantDTO participantDTO = new ParticipantDTO();
        participantDTO.setCode("PART05");
        participantDTO.setBic("AbCdEf12Gh3");
        participantDTO.setName("Example Company 123");
        participantDTO.setShortName("EXAMPLE");
        participantDTO.setType("Direct");
        participantDTO.setSettlementBank(null);

        return participantDTO;
    }




}