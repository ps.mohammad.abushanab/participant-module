package com.example.participant.management.controller;

import com.example.participant.management.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Test
    public void when_create_valid_login() {
        User loginUser = new User();
        loginUser.setPassword("123456");
        loginUser.setUsername("abushanab");
        ResponseEntity<Void> response = restTemplate.postForEntity("http://localhost:" + port + "/api/login", loginUser, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void when_create_invalid_login() {
        User loginUser = new User();
        loginUser.setPassword("invalidPassword11");
        loginUser.setUsername("invalidUsername");
        ResponseEntity<Void> response = restTemplate.postForEntity("http://localhost:" + port + "/api/login", loginUser, Void.class);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}