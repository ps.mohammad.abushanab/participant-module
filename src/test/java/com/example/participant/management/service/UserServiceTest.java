package com.example.participant.management.service;

import com.example.participant.management.dto.UserDTO;
import com.example.participant.management.exception.InvalidCredentialsException;
import com.example.participant.management.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;



@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;


    @Test
    public void testValidateLoginInput_ValidCredentials() {
     UserDTO user = userService.login("abushanab", "123456");

       assertNotNull(user);
    }

    @Test
    public void testValidateLoginInput_InvalidCredentials() {
        assertThrows(InvalidCredentialsException.class, () -> userService.login("invalidUser", "invalidPass"));
    }

    @Test
    public void testValidateLoginInput_EmptyUsername() {
        assertThrows(InvalidCredentialsException.class, () -> userService.login("", "somePassword"));
    }

    @Test
    public void testValidateLoginInput_EmptyPassword() {
        assertThrows(InvalidCredentialsException.class, () -> userService.login("someUsername", ""));
    }

    @Test
    public void testLogin_ValidCredentials() {

        assertThrows(InvalidCredentialsException.class, () -> userService.login("validUser", "validPass"));
    }

    @Test
    public void testLogin_InvalidCredentials() {
        assertThrows(InvalidCredentialsException.class, () -> userService.login("invalidUser", "invalidPass"));
    }
}