package com.example.participant.management.service;

import com.example.participant.management.dto.ParticipantDTO;
import com.example.participant.management.exception.*;
import com.example.participant.management.repository.ParticipantRepository;
import com.example.participant.management.validation.ParticipantValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@Import({ParticipantService.class, ParticipantValidation.class})
class ParticipantServiceTest {

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private ParticipantRepository participantRepository;


    private ParticipantDTO directParticipant ,indirectParticipant ;

    @BeforeEach
    void setUp() {
        directParticipant = new ParticipantDTO();
        directParticipant.setCode("PART01");
        directParticipant.setBic("AbCdEf12Gh3");
        directParticipant.setName("Mohammad");
        directParticipant.setShortName("MOHD123");
        directParticipant.setType("Direct");
        directParticipant.setLogo("company_logo.png");

        indirectParticipant = new ParticipantDTO();
        indirectParticipant.setCode("PART02");
        indirectParticipant.setBic("AbCdEf12Gh4");
        indirectParticipant.setName("Ahmad");
        indirectParticipant.setShortName("AHMD123");
        indirectParticipant.setType("Indirect");
        indirectParticipant.setLogo("company_logo.png");
        indirectParticipant.setSettlementBank("PART01");

    }

    @Test
    void when_create_valid_participant_should_return_notNull() {
        participantService.createParticipant(directParticipant);
        assertNotNull(participantService.getParticipantByCode(directParticipant.getCode()));
    }

    @Test
    void when_create_invalid_code_participant_should_return_exception() {
        ParticipantDTO participantDTO =directParticipant;
        participantService.createParticipant(directParticipant);
        assertThrows(ParticipantExistException.class, () -> participantService.createParticipant(participantDTO));
    }
    @Test
    void when_create_indirect_participant_should_return_notNull() {
        participantService.createParticipant(directParticipant);
        participantService.createParticipant(indirectParticipant);
        assertNotNull(participantService.getParticipantByCode(indirectParticipant.getCode()));
    }


    @Test
    void when_get_invalid_code_participant_should_return_exception() {
        String invalidCode = "PART12";
        participantService.createParticipant(directParticipant);
        assertThrows(ParticipantNotFoundException.class, () -> participantService.getParticipantByCode(invalidCode));
    }

    @Test
    void when_get_invalid_code_participant_should_return_valid_data() {
        participantService.createParticipant(directParticipant);
        ParticipantDTO result = participantService.getParticipantByCode(directParticipant.getCode());
        assertEquals(directParticipant.getCode(), result.getCode());
    }



    @Test
    void when_update_participant_name_should_return_valid_data() {

        participantService.createParticipant(directParticipant);
        directParticipant.setName("Updated Company Name");
        participantService.updateParticipant(directParticipant.getCode(), directParticipant);

        ParticipantDTO updatedParticipant = participantService.getParticipantByCode(directParticipant.getCode());
        assertEquals("Updated Company Name", updatedParticipant.getName());
    }

    @Test
    void when_get_all_participants_should_return_notNull() {
        participantService.createParticipant(directParticipant);
        List<ParticipantDTO> result = participantService.getAllParticipants();
        assertEquals(1, result.size());
    }



    @Test
    void when_delete_exist_participant_should_return_null() {
        participantService.createParticipant(directParticipant);
        participantService.deleteParticipant(directParticipant.getCode());
        assertThrows(ParticipantNotFoundException.class, () -> participantService.deleteParticipant(directParticipant.getCode()));
    }

    @Test
    void when_delete_not_exist_participant_should_return_exception() {
        assertThrows(ParticipantNotFoundException.class, () -> participantService.deleteParticipant("NotExist"));
    }

    @Test
    void when_delete_exist_participant_with_indirect_relationship_should_return_exception() {
        participantService.createParticipant(directParticipant);
        participantService.createParticipant(indirectParticipant);
        assertThrows(ParticipantDeletionException.class, () -> participantService.deleteParticipant(directParticipant.getCode()));

    }
}